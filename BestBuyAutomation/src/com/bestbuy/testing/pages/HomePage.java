package com.bestbuy.testing.pages;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.constants.ElementsConstants;
import com.bestbuy.testing.singleton.BestBuyWebDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class HomePage {
	public static WebDriver driver = BestBuyWebDriver.getDriver();
	public static WebDriverWait wait = BestBuyWebDriver.getWait();

	public static String loadHomePage() {
		Properties props = PropertyLoader.getDataProps();
		driver.get(props.getProperty(DataConstants.HOME_PAGE_URL));
		driver.manage().window().maximize();
//		try{
//		WebElement countryAndLanguage = wait.until(ExpectedConditions.visibilityOfElementLocated(
//				By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.COUNTRY_AND_lANGUAGE))));
//		countryAndLanguage.click();
//		Select country_language = new Select(countryAndLanguage);
//		country_language
//				.selectByVisibleText(PropertyLoader.getDataProps().getProperty(DataConstants.COUNTRY_AND_LANGUAGE));
//		WebElement go = wait.until(ExpectedConditions
//				.elementToBeClickable(By.xpath("//*[@id='intl_english']/div/div/div[1]/img")));
//		go.click();
//		}catch(Exception e){
//			
//		}
		return driver.getTitle();

	}

	public static void closePopUp() {

		try {
			WebElement close = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.CLOSE_BUTTON))));
			close.click();
		} catch (Exception e) {
			e.getMessage();
		}

	}

}
