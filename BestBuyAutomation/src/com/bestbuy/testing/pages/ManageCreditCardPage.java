package com.bestbuy.testing.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.constants.ElementsConstants;
import com.bestbuy.testing.singleton.BestBuyWebDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class ManageCreditCardPage {
	public static WebDriverWait wait=BestBuyWebDriver.getWait();
	public static WebDriver driver = BestBuyWebDriver.getDriver();
	
	//AddCreditCardPage
	public static void loadCreditCardPage(){
		 
		WebElement profile =wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SIGN_VERFIY))));
		profile.click();
		
		WebElement accountHome = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(PropertyLoader.getElementProps().getProperty(ElementsConstants.ACCOUNT_HOME))));
		accountHome.click();
		
		
		WebElement addcreditcardlink=wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(PropertyLoader.getElementProps().getProperty(ElementsConstants.ADD_CREDIT_CARD_LINK))));
		addcreditcardlink.click();
	}
		
	
	//CreditCardDetails
	public static void addCreditCardDetails() throws InterruptedException {
	
		WebElement creditCardNumber=wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.CREDIT_CARD_NUMBER))));
		creditCardNumber.click();
		creditCardNumber.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.CREDIT_CARD_NUMBER));
		
		WebElement creditmonth=wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.CREDIT_MONTH))));
		creditmonth.click();
		Select monthlist=new Select(creditmonth);
		monthlist.selectByVisibleText(PropertyLoader.getDataProps().getProperty(DataConstants.CREDIT_MONTH));
		
		WebElement credityear=wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.CREDIT_YEAR))));
		credityear.click();
		Select yearlist=new Select(credityear);
		yearlist.selectByVisibleText(PropertyLoader.getDataProps().getProperty(DataConstants.CREDIT_YEAR).toString());
		
		//BillingAddress
		
		WebElement creditFirstName =wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_FIRSTNAME))));
		creditFirstName.click();
		creditFirstName.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_FIRSTNAME));
		
		WebElement creditMiddleInitial =wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_MIDDLEINITIAL))));
		creditMiddleInitial.click();
		creditMiddleInitial.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_MIDDLEINITIAL));
		
		WebElement creditLastName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_LASTNAME))));
		creditLastName.click();
		creditLastName.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_LASTNAME));
		
		WebElement creditAddress1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_ADDRESS1))));
		creditAddress1.click();
		creditAddress1.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_ADDRESS1));
		
		WebElement creditAddress2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_ADDRESS2))));
		creditAddress2.click();
		creditAddress2.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_ADDRESS2));
		
		WebElement creditCity = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_CITY))));
		creditCity.click();
		creditCity.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_CITY));
		
		WebElement creditSate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_STATE))));
		Select credit_State = new Select(creditSate);
		credit_State.selectByVisibleText(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_STATE));
		
		WebElement creditZipCode = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_ZIPCODE))));
		creditZipCode.click();
		creditZipCode.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_ZIPCODE));
		
		WebElement creditPhone = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_PHONE))));
		creditPhone.click();
		creditPhone.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_PHONE));
		
		WebElement addCreditCardButton=wait.until(ExpectedConditions.elementToBeClickable(By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.ADD_CREDIT_CARD_BUTTON))));
		addCreditCardButton.submit();
		Thread.sleep(2000);
		
	
		WebElement keepaddressconfirm=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.KEEP_ADD_ADDRESS_CONFIRM))));
		//wait.until(ExpectedConditions.elementToBeClickable(keepaddressconfirm));
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("arguments[0].click();",keepaddressconfirm);
		keepaddressconfirm.sendKeys(Keys.ENTER);
		
}
	
	//AddCreditCardSuccMsg
	public static String addcreditsuccmsg(){
		
		WebElement creditCardsuccmsg=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.CREDIT_CARD_SUCC_MSG))));	
		wait.until(ExpectedConditions.textToBePresentInElement(creditCardsuccmsg, PropertyLoader.getDataProps().getProperty(DataConstants.CREDIT_CARD_SUCC_MSG))); 
		return creditCardsuccmsg.getText();
		
	}
		
	//RemoveCreditCard
	public static void removeCreditCard()  {
		
		
		WebElement removeCreditCardlink= wait.until(ExpectedConditions.elementToBeClickable(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.REMOVE_CREDIT_CARD_LINK))));
		removeCreditCardlink.click();
		
		//WebElement creditCardToRemove=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PropertyLoader.getElementProps().getProperty(ElementsConstants.ACCOUNT))));
	    WebElement creditCardToRemove=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='profileCreditCards']/div[2]/div/div[2]/div/div[1]/div/div[3]/div/div/div[2]/div[1]/span[2]")));
		String[] confirmCreditCardnumberarray=creditCardToRemove.getText().split("\\n",1);
		String confirmCreditCardnumber=confirmCreditCardnumberarray[0].replaceAll("\\n", " ");
				
	    if(confirmCreditCardnumber.equalsIgnoreCase(PropertyLoader.getDataProps().getProperty(DataConstants.CONFIRM_CREDIT_CARD_NUMBER))){
	    	 WebElement yesRemoveCreditCard=wait.until(ExpectedConditions.elementToBeClickable(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.YES_REMOVE_CREDIT_CARD))));
	 	    yesRemoveCreditCard.click();
	    }
	}
	    
	//RemoveCreditCardVerify
	public static String removeCardVerify(){
	   WebElement removeCardSuccMsg=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.REMOVE_CARD_SUCC_MSG))));
	   return removeCardSuccMsg.getText();
	   
	    
	}

}
