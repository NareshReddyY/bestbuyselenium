package com.bestbuy.testing.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.constants.ElementsConstants;
import com.bestbuy.testing.singleton.BestBuyWebDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class SignOnPage {
	public static WebDriver driver = BestBuyWebDriver.getDriver();
	public static WebDriverWait wait=BestBuyWebDriver.getWait();

	public static void loadSignInPage() {
			
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText(PropertyLoader.getElementProps().getProperty(ElementsConstants.ACCOUNT))));
			element.click();
			WebElement signInPage =  wait.until(ExpectedConditions.elementToBeClickable(By.linkText(PropertyLoader.getElementProps().getProperty(ElementsConstants.SIGN))));
			signInPage.click();

	}
	
	public static String loadSignInPageVerify(){
		return driver.getTitle();
	}

	public static void signIn() {

		   WebElement username = wait.until(ExpectedConditions.elementToBeClickable(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SIGN_USERNAME))));
			username.click();
			username.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.USERNAME));
			WebElement password = wait.until(ExpectedConditions.elementToBeClickable(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SIGN_PASSWORD))));
			password.click();
			password.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.PASSWORD));
			WebElement singinButton = wait.until(ExpectedConditions.elementToBeClickable(
					By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.SIGN_BUTTON))));
			singinButton.click();

		

	}

	public static String verifySignInStatus() {

		WebElement verifySignIn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SIGN_VERFIY))));
		return verifySignIn.getText().replaceAll("\\s","");

	}

	public static void signout() {
		WebElement profile = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SIGN_VERFIY))));
		wait.until(ExpectedConditions.elementToBeClickable(profile)).sendKeys(Keys.ENTER);
		
		WebElement signout =wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SIGN_OUT))));
		wait.until(ExpectedConditions.elementToBeClickable(signout)).sendKeys(Keys.ENTER);
		driver.close();

	}

	public static String signoutverify() {
		return driver.getTitle();
		
	}

}
