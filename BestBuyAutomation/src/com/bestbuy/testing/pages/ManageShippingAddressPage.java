package com.bestbuy.testing.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.constants.ElementsConstants;
import com.bestbuy.testing.singleton.BestBuyWebDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class ManageShippingAddressPage {

	public static WebDriver driver = BestBuyWebDriver.getDriver();
	public static WebDriverWait wait=BestBuyWebDriver.getWait();

	
	public static void loadAddShippingpage() {
		WebElement shippinglink = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.linkText(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_ADDRESS_LINK))));
		shippinglink.click();

	}
   public static String shippingpageverify() {
	   
		WebElement shippingTitle=wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_ADDRESS_TITLE))));
		return shippingTitle.getText();
	}
	
   public static void addshippingaddress()  {
		
		WebElement shippingFirstName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_FIRSTNAME))));
		shippingFirstName.click();
		shippingFirstName.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_FIRSTNAME));
		
		WebElement shippingMiddleInitial = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_MIDDLEINITIAL))));
		shippingMiddleInitial.click();
		shippingMiddleInitial.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_MIDDLEINITIAL));
		
		WebElement shippingLastName =wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_LASTNAME))));
		shippingLastName.click();
		shippingLastName.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_LASTNAME));
		
		WebElement shippingAddress1 =wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_ADDRESS1))));
		shippingAddress1.click();
		shippingAddress1.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_ADDRESS1));
		
		WebElement shippingAddress2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_ADDRESS2))));
		shippingAddress2.click();
		shippingAddress2.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_ADDRESS2));
		
		WebElement shippingCity = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_CITY))));
		shippingCity.click();
		shippingCity.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_CITY));
		
		WebElement shippingSate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_STATE))));
		Select shipping_State = new Select(shippingSate);
		shipping_State.selectByVisibleText(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_STATE));
		
		WebElement shippingZipCode = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_ZIPCODE))));
		shippingZipCode.click();
		shippingZipCode.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_ZIPCODE));
		
		WebElement shippingPhone = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_PHONE))));
		shippingPhone.click();
		shippingPhone.sendKeys(PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_PHONE));
		
		WebElement shippingSaveCheckBox = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_SAVE_CHECKBOX))));
		shippingSaveCheckBox.click();
		
		WebElement shippingSaveButton = wait.until(ExpectedConditions.elementToBeClickable(
				By.name(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_SAVE_BUTTON))));
		shippingSaveButton.submit();
		
		WebElement keepAddressButton =  wait.until(ExpectedConditions.elementToBeClickable(
				By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_KEEP_ADDRESS))));
		keepAddressButton.click();
	}

	
	public static String shipping_add_confirm_msg(){
		WebElement ship_add_conf_msg=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.SHIPPING_ADD_CONF_MSG))));
		return ship_add_conf_msg.getText();
	}
	
	
public static void remove_shipping_address()  {
	WebElement remove_ship_button=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.REMOVE_SHIP_BUTTON))));
	remove_ship_button.click();
	WebElement addresstoremove=wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(PropertyLoader.getElementProps().getProperty(ElementsConstants.ADDRESS_TO_REMOVE))));
	String[] remove_address_array=addresstoremove.getText().split("\\n",1);
	String  remove_address=remove_address_array[0].replaceAll("\\n", " ");
	String compare_remove=PropertyLoader.getDataProps().getProperty(DataConstants.ADDRESS_TO_REMOVE);
	
	if(remove_address.equalsIgnoreCase(compare_remove)){
		WebElement yes_remove=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.YES_TO_REMOVE) )));
		yes_remove.click();
		}
	
}
	
	public static String removeaddressverify(){
       wait.until(ExpectedConditions.invisibilityOfElementWithText(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.REMOVE_SUCC_MSG)),"Your primary shipping address has been saved."));
	WebElement remove_succmsg=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.REMOVE_SUCC_MSG))));
	//WebElement remove_succmsg=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[4]/div[1]/div/div")));
	wait.until(ExpectedConditions.textToBePresentInElement(remove_succmsg, PropertyLoader.getDataProps().getProperty(DataConstants.REMOVE_SHIP_CONFIRM_MSG)));
	return remove_succmsg.getText();
	}
	
	
}
