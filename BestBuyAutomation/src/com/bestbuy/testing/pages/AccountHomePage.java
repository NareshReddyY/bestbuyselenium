package com.bestbuy.testing.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bestbuy.testing.constants.ElementsConstants;
import com.bestbuy.testing.singleton.BestBuyWebDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class AccountHomePage {
	public static WebDriver driver = BestBuyWebDriver.getDriver();
	public static WebDriverWait wait=BestBuyWebDriver.getWait();

	public static void accountPage() {
//		try{
//			WebElement surveyPopUp=wait.until(ExpectedConditions.elementToBeClickable(By.id("survey_invite_no")));
//		    surveyPopUp.click();
//		}
//		
//		catch(Exception e){
//		
//		}
		WebElement profile =wait.until(ExpectedConditions.elementToBeClickable(By.id(PropertyLoader.getElementProps().getProperty(ElementsConstants.SIGN_VERFIY))));
		profile.click();
		WebElement accountHome = wait.until(ExpectedConditions.elementToBeClickable(By.linkText(PropertyLoader.getElementProps().getProperty(ElementsConstants.ACCOUNT_HOME))));
		accountHome.click();
	}

	public static String accountPageVerify() {
		WebElement welcomeWidgetGreeting = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.tagName(PropertyLoader.getElementProps().getProperty(ElementsConstants.WELCOME_WIDGET_GREETING))));
		String[] welcomeGreetingArray = welcomeWidgetGreeting.getText().split("\n", 1);
		String welcomeGreeting = welcomeGreetingArray[0].replaceAll("\\s|\\n", "");
		return welcomeGreeting;

	}
	
	public static String memeberIdVerify(){
		WebElement memberId=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.MEMBER_ID))));
		return memberId.getText().replaceAll("\\s","");
		
	}
	
	public static String rewardPointsVerify(){
		WebElement rewardpoints=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(PropertyLoader.getElementProps().getProperty(ElementsConstants.REWARD_POINTS))));
		return rewardpoints.getText();
		
	}
}
