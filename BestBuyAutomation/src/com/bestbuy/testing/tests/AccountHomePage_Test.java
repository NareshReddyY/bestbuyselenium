package com.bestbuy.testing.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.pages.AccountHomePage;
import com.bestbuy.testing.singleton.PropertyLoader;

public class AccountHomePage_Test {

	@Test
  public void testAccountPage()  {
		//AccountHomePage
		AccountHomePage.accountPage();
       
		//AccountPageVerify
	    AccountHomePage.accountPageVerify();
		Assert.assertEquals(AccountHomePage.accountPageVerify(), PropertyLoader.getDataProps().getProperty(DataConstants.ACCOUNT_VERIFY));
		
		//AccountMemberIdVerify
		AccountHomePage.memeberIdVerify();
		Assert.assertEquals(AccountHomePage.memeberIdVerify(),PropertyLoader.getDataProps().getProperty(DataConstants.MEMBER_ID));
		
		//AccountRewardsPointsVerify
		AccountHomePage.rewardPointsVerify();
		Assert.assertEquals(AccountHomePage.rewardPointsVerify(), PropertyLoader.getDataProps().getProperty(DataConstants.REWARD_POINTS));
		

}
}
