package com.bestbuy.testing.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.pages.ManageShippingAddressPage;
import com.bestbuy.testing.singleton.PropertyLoader;

public class ShippingAddress_Test {
  @Test
  public void testShippingAddress()   {
	  
	  // ValidateLoadingShippingPage
      ManageShippingAddressPage.loadAddShippingpage();
      
      //ValidateShippingPage
     // Assert.assertEquals(ManageShippingAddressPage.shippingpageverify(),PropertyLoader.getDataProps().getProperty(DataConstants.SHIPPING_PAGE_VERFIY));
     
      //AddingShippinAddress
      ManageShippingAddressPage.addshippingaddress();
      
      //ValidateADDShippingAddress
      Assert.assertEquals(ManageShippingAddressPage.shipping_add_confirm_msg(),PropertyLoader.getDataProps().getProperty(DataConstants.ADD_SHIP_CONFIRM_MSG));
      
      //ValidateRemoveShippingAddress
      ManageShippingAddressPage.remove_shipping_address();
      
      //ValidateShippingAddressRemoved
      Assert.assertEquals(ManageShippingAddressPage.removeaddressverify(),PropertyLoader.getDataProps().getProperty(DataConstants.REMOVE_SHIP_CONFIRM_MSG));
  }
}
