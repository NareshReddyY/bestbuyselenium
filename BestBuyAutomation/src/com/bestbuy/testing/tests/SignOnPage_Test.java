package com.bestbuy.testing.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.pages.HomePage;
import com.bestbuy.testing.pages.SignOnPage;
import com.bestbuy.testing.singleton.BestBuyWebDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class SignOnPage_Test {

	public static WebDriver driver = BestBuyWebDriver.getDriver();

	@Test
	public void test_signOn_page()  {

		//LoadHomePage
		Assert.assertEquals(HomePage.loadHomePage(),
				PropertyLoader.getDataProps().getProperty(DataConstants.HOME_PAGE_TITLE));
		//ClosePopUp
		HomePage.closePopUp();
		
		//LoadSigInPage
        SignOnPage.loadSignInPage();
       
        //ValidateSignInPage
		Assert.assertEquals(SignOnPage.loadSignInPageVerify(),
				PropertyLoader.getDataProps().getProperty(DataConstants.SIGN_IN_TITLE));

		//SigningIn
		SignOnPage.signIn();
		
		//ValidateSignedInStatus
		Assert.assertEquals(SignOnPage.verifySignInStatus(),
				PropertyLoader.getDataProps().getProperty(DataConstants.SIGN_IN_VERIFY));
		

         
         

	}

}