package com.bestbuy.testing.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.pages.ManageCreditCardPage;
import com.bestbuy.testing.pages.SignOnPage;
import com.bestbuy.testing.singleton.PropertyLoader;



public class CreditCardPage_Test {
  
	@Test
  public void testCreditCardPage() throws InterruptedException   {
		
		//LoadingCreditCardPage
		ManageCreditCardPage.loadCreditCardPage();
		
		//AddingCreditCardDetails
		ManageCreditCardPage.addCreditCardDetails();
		
		//ValidateCreditCardSuccMsg
		Assert.assertEquals(ManageCreditCardPage.addcreditsuccmsg(),PropertyLoader.getDataProps().getProperty(DataConstants.CREDIT_CARD_SUCC_MSG));
	     
		
		//RemovingCreditCardDetails
		ManageCreditCardPage.removeCreditCard();
		
		//ValidateCreditCardRemoveMsg
		Assert.assertEquals(ManageCreditCardPage.removeCardVerify(),PropertyLoader.getDataProps().getProperty(DataConstants.CREDIT_CARD_REMOVE_MSG));
	   
		//SignOut
		SignOnPage.signout();
		
  }
}
