package com.bestbuy.testing.constants;

public class DataConstants {
	
	public static final String COUNTRY_AND_LANGUAGE="countryandLanguage";
	public static final String HOME_PAGE_URL="homePageUrl";
	public static final String HOME_PAGE_TITLE="homePageTitle";
	public static final String SIGN_IN_TITLE="signInPageTitle";
	public static final String USERNAME="userName";
	public static final String PASSWORD="password";
	public static final String SIGN_IN_VERIFY="signInVerify";
	public static final String SIGN_OUT_VERIFY="signOutVerify";
	public static final String ACCOUNT_VERIFY="account_verify";
	public static final String MEMBER_ID="memberid";
	public static final String REWARD_POINTS="rewardPoints";
	public static final String SHIPPING_PAGE_VERFIY="ship_page_verify";
	public static final String ADD_SHIPPING_PAGE_VERIFY="add_ship_page_verify";
	public static final String SHIPPING_FIRSTNAME="ship_first_name";
	public static final String SHIPPING_MIDDLEINITIAL="ship_mid_initial";
	public static final String SHIPPING_LASTNAME="ship_last_name";
	public static final String SHIPPING_ADDRESS1="ship_address1";
	public static final String SHIPPING_ADDRESS2="ship_address2";
	public static final String SHIPPING_CITY="ship_city";
	public static final String SHIPPING_STATE="ship_state";
	public static final String SHIPPING_ZIPCODE="ship_zip_code";
	public static final String SHIPPING_PHONE="ship_phone";
	public static final String ADD_SHIP_CONFIRM_MSG="ship_add_confirm_msg";
	public static final String REMOVE_SHIP_CONFIRM_MSG="ship_remove_confirm_msg";
	public static final String ADDRESS_TO_REMOVE="address_to_remove";
	public static final String CREDIT_CARD_NUMBER="creditcardnumber";
	public static final String CREDIT_MONTH="creditmonth";
	public static final String CREDIT_YEAR="credityear";
	public static final String CREDIT_CARD_SUCC_MSG="credit_card_succ_msg";
	public static final String CONFIRM_CREDIT_CARD_NUMBER="confirmCreditCardnumber";
	public static final String CREDIT_CARD_REMOVE_MSG="credit_card_remove_msg";
	
	
	
	
	
	

}
