package com.bestbuy.testing.constants;

public class ElementsConstants {
 
	public static final String COUNTRY_AND_lANGUAGE="countryandLanguage";
	public static final String GO="go";
	public static final String CLOSE_BUTTON="close_button";
	public static final String ACCOUNT="Account_Click";
	public static final String SIGN="Sign_In";
	public static final String SIGN_USERNAME="Sign_In_Username";
	public static final String SIGN_PASSWORD="Sign_In_Password";
	public static final String SIGN_BUTTON="Sign_In_Button";
	public static final String SIGN_VERFIY="Sign_Verify_Id";
	public static final String SIGN_OUT="Sign_Out_Id";
	public static final String ACCOUNT_HOME="account_Home";
	public static final String WELCOME_WIDGET_GREETING="welcomegreeting";
	public static final String MEMBER_ID="member_ID";
	public static final String REWARD_POINTS="reward_Points";
	public static final String SHIPPING_ADDRESS_LINK="shipping_address_link";
	public static final String SHIPPING_ADDRESS_TITLE="shipping_title";
	public static final String SHIPPING_ADD_ADDRESS_BUTTON="shipping_add_button";
	public static final String SHIPPING_FIRSTNAME="ship_first_name";
	public static final String SHIPPING_MIDDLEINITIAL="ship_mid_initial";
	public static final String SHIPPING_LASTNAME="ship_last_name";
	public static final String SHIPPING_ADDRESS1="ship_address1";
	public static final String SHIPPING_ADDRESS2="ship_address2";
	public static final String SHIPPING_CITY="ship_city";
	public static final String SHIPPING_STATE="ship_state";
	public static final String SHIPPING_ZIPCODE="ship_zip_code";
	public static final String SHIPPING_PHONE="ship_phone";
	public static final String SHIPPING_SAVE_CHECKBOX="ship_save_checkbox";
	public static final String SHIPPING_SAVE_BUTTON="ship_save_button";
	public static final String SHIPPING_KEEP_ADDRESS="ship_keep_address";
	public static final String SHIPPING_CANCEL_BUTTON="ship_cancel_button";
	public static final String SHIPPING_ADD_CONF_MSG="ship_add_confirm_msg";
	public static final String REMOVE_SHIP_BUTTON="remove_ship_button";
	public static final String ADDRESS_TO_REMOVE="addresstoremove";
	public static final String YES_TO_REMOVE="yes_remove";
	public static final String REMOVE_SUCC_MSG="remove_succmsg";
	public static final String ADD_CREDIT_CARD_LINK="addcreditcardlink";
	public static final String CANCEL_CREDIT_LINK="cancelcreditcardlink";
	public static final String ADD_CREDIT_CARD_AFTER_CANCEL="addcreditcardaftercancel";
	public static final String CREDIT_CARD_NUMBER="creditCardNumber";
	public static final String CREDIT_MONTH="creditmonth";
	public static final String CREDIT_YEAR="credityear";
	public static final String ADD_CREDIT_CARD_BUTTON="addCreditCardButton";
	public static final String KEEP_ADD_ADDRESS_CONFIRM="keepaddressconfirm";
	public static final String CREDIT_CARD_SUCC_MSG="creditCardsuccmsg";
	public static final String REMOVE_CREDIT_CARD_LINK="removeCreditCardlink";
	public static final String CARD_TO_REMOVE="creditCardToRemove";
	public static final String YES_REMOVE_CREDIT_CARD="yesRemoveCreditCard";
	public static final String REMOVE_CARD_SUCC_MSG="removeCardSuccMsg";
	
	
	
	
	
	
	
}
