package com.bestbuy.testing.singleton;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {

	//private static Properties props;

	private static Properties dataProps;

	private static Properties elementProps;

	private PropertyLoader() {

	}

//	public static Properties getProps() {
//		if (props == null) {
//			FileInputStream stream1;
//			FileInputStream stream2;
//			try {
//				stream1 = new FileInputStream("data.properites");
//				stream2 = new FileInputStream("elements.properites");
//				props = new Properties();
//				props.load(stream1);
//				props.load(stream2);
//			} catch (FileNotFoundException e) {
//
//				e.printStackTrace();
//			} catch (IOException e) {
//
//				e.printStackTrace();
//			}
//		}
//		return props;
//	}

	public static Properties getDataProps() {
		if (dataProps == null) {
			FileInputStream stream;
			try {
				stream = new FileInputStream("data.properties");
				dataProps = new Properties();
				dataProps.load(stream);
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		return dataProps;
	}

	public static Properties getElementProps() {
		if (elementProps == null) {
			FileInputStream stream;
			try {
				stream = new FileInputStream("elements.properties");
				elementProps = new Properties();
				elementProps.load(stream);
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		return elementProps;
	}

}
