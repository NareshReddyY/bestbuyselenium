package com.bestbuy.testing.singleton;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BestBuyWebDriver {
	
	private static  WebDriver driver;
	private static  WebDriverWait wait;
	
	private BestBuyWebDriver(){
		
	}
   
	public static WebDriver getDriver(){
		if(driver==null){
			System.setProperty("webdriver.chrome.driver", "C://Users//ynare//Downloads//Automation//chromedriver.exe");
			ChromeOptions options=new ChromeOptions();
			Map<String,Object> prefs=new HashMap<String,Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled",false);
			options.setExperimentalOption("prefs", prefs);
			driver=new ChromeDriver(options);

		}
		return driver;
	}
	
	public static WebDriverWait getWait(){
		wait=new WebDriverWait(driver, 30);
		return wait;
	}
}
